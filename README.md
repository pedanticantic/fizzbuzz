fizzbuzz
========

Solution to Fizz Buzz game.

This is my first repo on GitHub, so it's really just an experiment.

Fizz Buzz
---------

This is a counting game where a group of people take it in turns to say the next number - 1, 2, 3, 4, etc.

However, there is of course a twist...
if the number is divisible by 3, you say "fizz".
if the number is divisible by 5, you say "buzz".
And you can also have the rule that if the number is divisible by 7, you say "whizz".
If a number matches more than one rule, you say all the relevant words.

So, the sequence goes: 1, 2, fizz, 4, buzz, fizz, 7, 8, fizz, buzz, 11, fizz, 13, 14, fizz buzz, 16, 17, fizz...

It's quite a good test for job interviews.