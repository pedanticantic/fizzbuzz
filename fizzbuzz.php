<?php

/*
** This code emulates the Fizz Buzz game (http://en.wikipedia.org/wiki/Fizz_buzz)
** The start and end numbers are defined as constants.
** The divisors and associated words are represented as an array, so they can be amended,
** added to, or removed as necessary.
** The code is designed to be run from the command line.
**
*/

// Define the start and end numbers.
define( 'FROM', 1 );
define( 'TO', 110 );

// The array of divisors and words. The array is indexed by the divisor.
$divisors_words = array( 3 => 'Fizz',
                         5 => 'Buzz',
                         7 => 'Whizz'
                       );

echo '<pre >';
// Output a title.
echo "\n";
echo "Fizz-Buzz\n";
echo "---------\n";

// Loop through the numbers from FROM to TO.
for( $idx = FROM ; $idx <= TO ; $idx++ ) {
    
    // Initialise the output for this number.
    $word_out = '';
    
    // Loop through the divisors, and add any words as necessary.
    foreach( $divisors_words as $divisor => $word ) {
        if ( 0 == $idx % $divisor ) {
            $word_out .= $word . ' ';  // Note the trailing space to separate multiple words.
        }
    }
    
    // If no words were added, the output is just the number; Otherwise remove the last trailing space.
    if ( '' == $word_out ) {
        $word_out = $idx;
    } else {
        $word_out = rtrim( $word_out );
    }
    
    // Output this word/number.
    echo $word_out . "\n";
    
}

echo '</pre>';

?>
